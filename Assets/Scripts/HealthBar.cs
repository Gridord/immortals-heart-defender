﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    private Image bar;

    public float max { get; set; }
    private float Value;
    public float value
    {
        get { return Value; }
        set
        {
            Value = Mathf.Clamp(value, 0, max);
            bar.fillAmount = (1 / max) * Value;
        }
    }
    
    void Awake () {
        bar = GetComponent<Image>();
	}
}
