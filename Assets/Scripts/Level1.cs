﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level1 : MonoBehaviour {
    
    public GameObject[] dialogPrefabs;
    public int[] posToStartDialogs;
    public GameObject healthBossDisplayPanel;
    public GameObject dialogDisplayPanel;
    private Pause pause;
    private EnemyArmy enemyArmy;
    private int indexDialog = 0;

    void Start ()
    {
        healthBossDisplayPanel.SetActive(false);
        enemyArmy = GameObject.Find("EnemyArmy").GetComponent<EnemyArmy>();
        pause = GameObject.Find("Pause").GetComponent<Pause>();
        pause.PauseGame();
    }
	
	void FixedUpdate ()
    {
        if (!pause.inPause)
        {
            if (indexDialog < posToStartDialogs.Length && indexDialog < dialogPrefabs.Length && indexDialog >= 0)
            {
                if (enemyArmy.pos >= posToStartDialogs[indexDialog])
                {
                    GameObject dialog = (GameObject)Instantiate(dialogPrefabs[indexDialog], transform.position, Quaternion.identity);
                    dialog.transform.parent = transform;
                    indexDialog++;
                }
            }
        }
	}


}
