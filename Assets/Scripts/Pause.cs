﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {

    public GameObject pausePanel;
    private bool axisInUse = false;
    public bool inPause { get; set; }

    void Start()
    {
        pausePanel = GameObject.Find("PausePanel");
        pausePanel.SetActive(false);
    }

    void Update()
    {
        if (Input.GetAxisRaw("Cancel") != 0)
        {
            if (!axisInUse)
            {
                if (!pausePanel.activeInHierarchy)
                {
                    PauseGame();
                }
                else
                {
                    ContinueGame();
                }
                axisInUse = true;
            }
        }
        else
        {
            axisInUse = false;
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
        inPause = true;
    }

    public void ContinueGame()
    {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
        inPause = false;
    }
}
