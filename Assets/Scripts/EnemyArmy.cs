﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArmy : MonoBehaviour {

    private GameObject levelManager;
    public GameObject[] enemyFormations;
    public float[] spawnDelays;
    public int pos = 0;

    private int nbFormationsStillAlive;
    
	void Start ()
    {
        levelManager = GameObject.Find("LevelManager");
        nbFormationsStillAlive = 0;

        if (enemyFormations.Length > 0)
        {
            if (spawnDelays.Length > 0 && spawnDelays[0] > 0.0f) { Invoke("SpawnNextFormation", spawnDelays[0]); }
            else { SpawnNextFormation(); }
        }
        else
        {
            levelManager.GetComponent<LevelManager>().LoadLevel("Win_Screen");
        }
        
    }

    public void NotifyFormationDeath()
    {
        nbFormationsStillAlive--;
        if(pos >= 0)
        {
            if(spawnDelays.Length - 1 <= pos && nbFormationsStillAlive <= 0)
            {
                nbFormationsStillAlive = 0;
                SpawnNextFormation();
            }
            else if(pos < enemyFormations.Length)
            {
                if (spawnDelays[pos] == -1 && nbFormationsStillAlive <= 0)
                {
                    nbFormationsStillAlive = 0;
                    SpawnNextFormation();
                }
            }
        }
    }

    void SpawnNextFormation()
    {
        if (pos < enemyFormations.Length)
        {
            GameObject enemyFormation = (GameObject)Instantiate(enemyFormations[pos], transform.position, Quaternion.identity);
            enemyFormation.transform.parent = transform;
            pos++;
            nbFormationsStillAlive++;
            if (spawnDelays.Length > pos)
            {
                if (spawnDelays[pos] > 0.0f) { Invoke("SpawnNextFormation", spawnDelays[pos]); }
                else if (spawnDelays[pos] == 0.0f) { SpawnNextFormation(); }
            }
        }
        else
        {
            pos++;//pour finir à pos = enemyFormations.Length + 1 afin de finir avec le dialogue de fin
        }
        
    }
}
