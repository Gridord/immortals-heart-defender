﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogScript : MonoBehaviour {

    public float speedOfDialog = 0.1f;
    [TextArea(3, 10)]
    public string[] dialogTexts;
    public Color[] dialogTextColors;
    public int[] indexColorForText;
    public bool lastDialog = false;

    private GameObject dialogPanel;
    private GameObject dialogText;
    private GameObject continueDialog;
    private bool axisInUse = false;

    // Use this for initialization
    void Start ()
    {
        dialogPanel = GameObject.Find("Level1").GetComponent<Level1>().dialogDisplayPanel;
        dialogPanel.SetActive(true);
        dialogText = GameObject.Find("DialogText");
        continueDialog = GameObject.Find("ContinueDialogText");

        foreach(GameObject shot in GameObject.FindGameObjectsWithTag("Shots"))
        {
            Destroy(shot);
        }

        StartCoroutine(Dialog());
    }
	
    IEnumerator Dialog()
    {
        Time.timeScale = 0;

        int i = 0;
        
        foreach (string d in dialogTexts){
            dialogText.GetComponent<Text>().text = "";

            if (indexColorForText.Length > i && dialogTextColors.Length > indexColorForText[i])
            {
                dialogText.GetComponent<Text>().color = dialogTextColors[indexColorForText[i]];
            }
            else
            {
                dialogText.GetComponent<Text>().color = Color.white;
            }
            continueDialog.GetComponent<Text>().color = dialogText.GetComponent<Text>().color;

            i++;

            foreach (char c in d)
            {
                continueDialog.SetActive(false);
                dialogText.GetComponent<Text>().text += c;
                if(!axisInUse && Input.GetAxisRaw("Fire1") != 0)
                {
                    dialogText.GetComponent<Text>().text = d;
                    axisInUse = true;
                    break;
                }
                else
                {
                    if (Input.GetAxisRaw("Fire1") == 0)
                    {
                        axisInUse = false;
                    }
                    if (speedOfDialog > 0)
                    {
                        yield return new WaitForSecondsRealtime(speedOfDialog);
                    }
                }
            }
            continueDialog.SetActive(true);

            while (axisInUse || Input.GetAxisRaw("Fire1") == 0)
            {
                if(Input.GetAxisRaw("Fire1") == 0) { axisInUse = false; }
                yield return null;
            }
            axisInUse = true;
        }

        dialogPanel.SetActive(false);
        Time.timeScale = 1;
        
        if (lastDialog)
        {
            GameObject.Find("LevelManager").GetComponent<LevelManager>().LoadLevel("Win_Screen");
        }

        Destroy(gameObject);
    }
}
