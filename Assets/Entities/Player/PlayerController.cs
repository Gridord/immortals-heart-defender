﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float health = 100.0f;
    public GameObject healthDisplayBar;
    private float maxHealth;
    public float speed = 7.0f;
    public float speedPlusForDiag = 5.0f;

    public GameObject fireShotPrefab;
    public GameObject iceShotPrefab;
    public float fireRateShoot = 0.5f;
    public float iceRateShoot = 1.0f;
    public float paddingShot = 1.0f;
    private float fireTimeCum = 0.0f;

    public GameObject limits;

    private GameObject levelManager;
    public string loseSceneName;

    private void Start()
    {
        levelManager = GameObject.Find("LevelManager");
        fireTimeCum = iceRateShoot;
        maxHealth = health;
        healthDisplayBar.GetComponent<HealthBar>().max = maxHealth;
        healthDisplayBar.GetComponent<HealthBar>().value = maxHealth;
    }

    void FixedUpdate()
    {
        HandleTargeting();
        HandleMovements();
        HandleShooting();
    }

    void HandleTargeting()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, transform.position - mousePos);
    }

    void HandleMovements()
    {
        float directionX = Input.GetAxis("Horizontal");
        float directionY = Input.GetAxis("Vertical");
        
        if(Mathf.Abs(directionX) + Mathf.Abs(directionY) > 0)
        {
            Vector3 oldPos = gameObject.GetComponent<Transform>().position;
            Vector3 initialPos = limits.transform.position;
            Vector2 offset = limits.GetComponent<CircleCollider2D>().offset;
            initialPos = new Vector3(initialPos.x + offset.x, initialPos.y + offset.y, oldPos.z);

            float sharedSpeed = speed / (Mathf.Abs(directionX) + Mathf.Abs(directionY));
            sharedSpeed += speedPlusForDiag;

            float newX = oldPos.x + directionX * sharedSpeed * Time.deltaTime;
            float newY = oldPos.y + directionY * sharedSpeed * Time.deltaTime;

            Vector3 newPos = new Vector3(newX, newY, oldPos.z);

            Vector3 allowedPos = newPos - initialPos;
            allowedPos = Vector3.ClampMagnitude(allowedPos, limits.GetComponent<CircleCollider2D>().radius);

            gameObject.GetComponent<Transform>().position = initialPos + allowedPos;
        }
    }

    void HandleShooting()
    {
        if (Input.GetAxis("Fire1") > 0.01 && fireTimeCum >= fireRateShoot)
        {
            Fire(fireShotPrefab);
            fireTimeCum = 0.0f;
        }
        else if (Input.GetAxis("Fire2") > 0.01 && fireTimeCum >= iceRateShoot)
        {
            Fire(iceShotPrefab);
            fireTimeCum = 0.0f;
        }
        else
        {
            fireTimeCum += Time.deltaTime;
        }
    }

    void Fire(GameObject shotPrefab)
    {
        Vector3 shotPos = new Vector3(transform.position.x - this.transform.up.x * paddingShot, transform.position.y - this.transform.up.y * paddingShot, transform.position.z);
        GameObject shot = (GameObject)Instantiate(shotPrefab, shotPos, Quaternion.identity);

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        shot.transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePos - transform.position);

        float shotSpeed = shotPrefab.GetComponent<Projectile>().speed;
        shot.GetComponent<Rigidbody2D>().velocity = new Vector3(-this.transform.up.x * shotSpeed, -this.transform.up.y * shotSpeed, 0);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Projectile missile = collider.gameObject.GetComponent<Projectile>();
        if (missile != null)
        {
            health -= missile.GetDamage();
            missile.Hit();

            gameObject.GetComponent<AudioSource>().Play();

            healthDisplayBar.GetComponent<HealthBar>().value = health;
            
            if (health <= 0.0f)
            {
                levelManager.GetComponent<LevelManager>().LoadLevel(loseSceneName);
            }
        }
    }
}
