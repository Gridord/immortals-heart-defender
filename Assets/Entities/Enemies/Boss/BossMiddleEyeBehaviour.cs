﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMiddleEyeBehaviour : MonoBehaviour {

    public GameObject deathFogPrefab;
    public float health = 4000.0f;
    public GameObject quickShotPrefab;
    public GameObject bigShotPrefab;
    public float quickRateShoot = 4.0f;
    public float bigRateShoot = 10.0f;
    public float paddingShot = 1.0f;
    private float quickFireTimeCum = 0.0f;
    private float bigFireTimeCum = 0.0f;

    private GameObject player;

    void Start()
    {
        player = GameObject.Find("Player");
        quickFireTimeCum = 0.0f;
        bigFireTimeCum = 0.0f;
    }

    void Update()
    {
        HandleTargeting();
        HandleShooting();
    }

    void HandleTargeting()
    {
        Vector3 playerPos = player.transform.position;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, transform.position - playerPos);
    }

    void HandleShooting()
    {
        if (quickFireTimeCum >= quickRateShoot)
        {
            Fire(quickShotPrefab);
            quickFireTimeCum = 0.0f;
        }
        else
        {
            quickFireTimeCum += Time.deltaTime;
        }

        if (bigFireTimeCum >= bigRateShoot)
        {
            Fire(bigShotPrefab);
            bigFireTimeCum = 0.0f;
        }
        else
        {
            bigFireTimeCum += Time.deltaTime;
        }
    }

    void Fire(GameObject shotPrefab)
    {
        Vector3 shotPos = new Vector3(transform.position.x - this.transform.up.x * paddingShot, transform.position.y - this.transform.up.y * paddingShot, transform.position.z);
        GameObject shot = (GameObject)Instantiate(shotPrefab, shotPos, Quaternion.identity);

        Vector3 playerPos = player.transform.position;
        shot.transform.rotation = Quaternion.LookRotation(Vector3.forward, playerPos - transform.position);

        float shotSpeed = shotPrefab.GetComponent<Projectile>().speed;
        shot.GetComponent<Rigidbody2D>().velocity = new Vector3(-this.transform.up.x * shotSpeed, -this.transform.up.y * shotSpeed, 0);
    }



    void OnTriggerEnter2D(Collider2D collider)
    {
        Projectile missile = collider.gameObject.GetComponent<Projectile>();
        if (missile != null)
        {
            health -= missile.GetDamage();
            missile.Hit();
            if (health <= 0.0f)
            {
                Instantiate(deathFogPrefab, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}
