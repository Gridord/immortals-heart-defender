﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRightEyeBehaviour : MonoBehaviour {

    public GameObject deathFogPrefab;
    public float health = 3000.0f;
    public GameObject shotPrefab;
    public float rateShoot = 2.0f;
    public float paddingShot = 1.0f;
    private float fireTimeCum = 0.0f;

    private GameObject player;

    void Start()
    {
        player = GameObject.Find("Player");
        fireTimeCum = 0.0f;
    }

    void Update()
    {
        HandleTargeting();
        HandleShooting();
    }

    void HandleTargeting()
    {
        Vector3 playerPos = player.transform.position;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, transform.position - playerPos);
    }

    void HandleShooting()
    {
        if (fireTimeCum >= rateShoot)
        {
            Fire(shotPrefab);
            fireTimeCum = 0.0f;
        }
        else
        {
            fireTimeCum += Time.deltaTime;
        }
    }

    void Fire(GameObject shotPrefab)
    {
        Vector3 shotPos = new Vector3(transform.position.x - this.transform.up.x * paddingShot, transform.position.y - this.transform.up.y * paddingShot, transform.position.z);
        GameObject shot = (GameObject)Instantiate(shotPrefab, shotPos, Quaternion.identity);

        Vector3 playerPos = player.transform.position;
        shot.transform.rotation = Quaternion.LookRotation(Vector3.forward, playerPos - transform.position);

        float shotSpeed = shotPrefab.GetComponent<Projectile>().speed;
        shot.GetComponent<Rigidbody2D>().velocity = new Vector3(-this.transform.up.x * shotSpeed, -this.transform.up.y * shotSpeed, 0);
    }



    void OnTriggerEnter2D(Collider2D collider)
    {
        Projectile missile = collider.gameObject.GetComponent<Projectile>();
        if (missile != null)
        {
            health -= missile.GetDamage();
            missile.Hit();
            if (health <= 0.0f)
            {
                Instantiate(deathFogPrefab, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}
