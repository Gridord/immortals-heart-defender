﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehaviour : MonoBehaviour {

    public GameObject deathFogPrefab;
    public float liftUpCamera;
    public GameObject healthDisplayPanel;
    private float health;
    private float maxHealth;
    private GameObject leftEye;
    private GameObject middleEye;
    private GameObject rightEye;

    void Start () {
        Vector3 oldPos = Camera.main.transform.position;
        Vector3 newPos = new Vector3(oldPos.x, oldPos.y + liftUpCamera, oldPos.z);
        Camera.main.transform.position = newPos;
        
        leftEye = GameObject.Find("BossLeftEye");
        middleEye = GameObject.Find("BossMiddleEye");
        rightEye = GameObject.Find("BossRightEye");

        health = 0.0f;
        health += leftEye.GetComponent<BossLeftEyeBehaviour>().health;
        health += middleEye.GetComponent<BossMiddleEyeBehaviour>().health;
        health += rightEye.GetComponent<BossRightEyeBehaviour>().health;

        healthDisplayPanel = GameObject.Find("Level1").GetComponent<Level1>().healthBossDisplayPanel;
        healthDisplayPanel.SetActive(true);
        maxHealth = health;
        healthDisplayPanel.GetComponentInChildren<HealthBar>().max = maxHealth;
        healthDisplayPanel.GetComponentInChildren<HealthBar>().value = maxHealth;
    }

    private void Update()
    {
        health = 0.0f;
        if (leftEye != null) { health += leftEye.GetComponent<BossLeftEyeBehaviour>().health; }
        if (middleEye != null) { health += middleEye.GetComponent<BossMiddleEyeBehaviour>().health; }
        if (rightEye != null) { health += rightEye.GetComponent<BossRightEyeBehaviour>().health; }

        healthDisplayPanel.GetComponentInChildren<HealthBar>().value = health;

        if (health <= 0.0f)
        {
            foreach(Transform sibling in transform.parent.transform.parent)
            {
                Destroy(sibling.gameObject);
            }
            healthDisplayPanel.gameObject.SetActive(false);
            Instantiate(deathFogPrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
    
}
