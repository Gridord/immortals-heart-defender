﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFormation : MonoBehaviour {

    public GameObject[] enemyPrefab;
    public float[] spawnDelays;
    private GameObject enemyArmy;

    
    void Start () {
        enemyArmy = GameObject.Find("EnemyArmy");
        if(enemyPrefab.Length > 0)
        {
            StartCoroutine(SpawnEnemy());
        }
        
    }
    
    void Update () {
        if (AllMembersAreDead())
        {
            enemyArmy.GetComponent<EnemyArmy>().NotifyFormationDeath();
            Destroy(gameObject);
        }
    }

    bool AllMembersAreDead()
    {
        foreach (Transform childPositionGameObject in transform)
        {
            if (childPositionGameObject.childCount > 0)
            {
                return false;
            }
        }
        return true;
    }

    IEnumerator SpawnEnemy()
    {
        int pos = 0;
        foreach (Transform child in transform)
        {
            if (spawnDelays.Length > pos && spawnDelays[pos] > 0) { yield return new WaitForSeconds(spawnDelays[pos]); }
            GameObject enemy = (GameObject)Instantiate(enemyPrefab[pos], transform.GetChild(pos).position, Quaternion.identity);
            enemy.transform.parent = transform.GetChild(pos);
            pos++;
        }
        
    }
}
